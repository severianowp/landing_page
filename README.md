# Landing Page Transistor

Criação de layout de apresentação do jogo Transistor.

## Ferramentas Utilizadas no desenvolvimento

- Vue.js
- TypeScript
- Vuetify
- SCSS

Para ver a versão de produção, basta entra nesse link da Netfly:

<https://flamboyant-bartik-c0a56e.netlify.app>

Caso queira executar a versão de desenvolvimento, basta seguir os comandos abaixo:

## Requisitos:

- Node instalado

### Comandos para a compilação e execução do layout com vue.js

dentro da pasta, execute os seguintes comandos:

```
npm install

npm run serve

```

Quando terminar o processo do `npm run serve`, o terminal mostrará a url para acessar o site de forma local.
